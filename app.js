const express = require("express");
const app = express();
const AWS = require("aws-sdk");
require('dotenv').config();
const port = process.env.PORT || 3000;

const knex = require('knex')({
    client: 'mysql',
    connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE
    }
});


AWS.config.getCredentials(function(err) {
    if (err) console.log(err.stack);
});

// Set the region
AWS.config.update({ region: "Global Endpoint" });
var iam = new AWS.IAM({ apiVersion: "2010-05-08" });


// Read route Begins

app.get("/", function(req, res) {
    knex.from('iam-users').select("*")
        .then((rows) => {
            res.json(rows);
        }).catch((err) => { console.log(err); throw err })
});

// Read Route Ends

// delete Route - Begins

app.delete("/delete/:uname", function(req, res) {
    knex("iam-users").where({ username: req.params.uname })
        .del()
        .then(function() {
            res.json({ deleted: 'success in db' })
        })
        .catch((err) => {
            console.log(err);
            throw err;
        })
    var params = {
        UserName: req.params.uname,
    };
    iam.getUser(params, function(err, data) {
        if (err && err.code === "NoSuchEntity") {
            console.log("User " + req.params.uname + " does not exist.");
        } else {
            iam.deleteUser(params, function(err, data) {
                if (err) {
                    console.log("Error", err);
                } else {
                    console.log("Success from AWS", data);
                }
            });
        }
    });
})



// delete Route - Ends


// Update or Sync IAM  

app.post("/sync", function(req, res) {
    iam.listUsers(function(err, data) {
        if (err) {
            console.log("Error", err);
        } else {
            var users = data.Users || [];
            // stringify(Users);
            users.forEach(function(user) {
                const AllData = [{
                    userid: user.UserId,
                    username: user.UserName,
                    arn: user.Arn
                }]

                knex(`iam-users`)
                    .insert(AllData)
                    .then(() => console.log("data inserted"))
                    .catch((err) => {
                        console.log(err);
                        throw err;
                    })

                // console.log(`User ${user.UserName}  userid ${user.UserId}  and arn ${user.Arn}`);
            });
        }
    })
    res.send('Data Updated')

})

module.exports = app;
